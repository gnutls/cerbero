# Using cerbero to cross-compile gnutls on Android

This is based on instructions provided [by Andoni Morales](http://lists.gnu.org/archive/html/help-libtasn1/2013-01/msg00002.html).

Steps to follow:
1. Download Cerbero 
```
$ git clone git://anongit.freedesktop.org/gstreamer-sdk/cerbero
```
2. Bootstrap. This downloads the toolchain and setup your system with all the required elements to start the build.
```
$ ./cerbero-uninstalled -c config/cross-android.cbc bootstrap
```
3. Update the recipes for gnutls with the ones here
4. Build individual recipes (optional)
```
$ ./cerbero-uninstalled -c config/cross-android.cbc build gnutls
```
5. Build packages
```
$ ./cerbero-uninstalled -c config/cross-android.cbc package gnutls-bin
```

There is also a shell command that you can use if you want to hack on
a project instead of building the recipe
```
$ ./cerbero-uninstalled -c config/cross-android.cbc shell
$ ./configure --prefix /home/username/cerbero/dist/android_arm --libdir /home/username/cerbero/dist/android_arm/lib \
	--disable-maintainer-mode  --disable-silent-rules  --host=arm-linux-androideabi \
	--without-zlib --without-default-trust-store-file --enable-local-libopts --disable-guile \
	--disable-nls --without-libiconv --disable-doc --disable-crywrap --disable-libdane \
	--disable-cxx  --disable-openssl-compatibility --without-p11-kit
```
